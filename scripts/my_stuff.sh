# exports
export PATH="${HOME}/.local/bin:${PATH}"
export EDITOR=nvim

# virtualenvwrapper
export PROJECT_HOME="${HOME}"/projects
export WORKON_HOME="${HOME}"/.venvs
export VIRTUALENVWRAPPER_SCRIPT=/usr/share/virtualenvwrapper/virtualenvwrapper.sh

# sources

# virtualenvwrapper
source /usr/share/virtualenvwrapper/virtualenvwrapper_lazy.sh
source "${HOME}"/.cargo/env

# functions
function mot {
	fm6000 -L 20 -c random
}

function hello {
	fm6000 -L 20 -c random -say "Hello MOTHAFUCKA"
}

function genpwd {
	head -c "$1" /dev/urandom | base64
}

function scanhost {
	clear
	doas nmap -sS -P0 -O $1
}

function scannet {
	clear
	doas nmap -sP $1
}

function refresh {
	clear
	source "${HOME}"/.zshrc
}

function background {
	while true; 
	do
		PICTURE=$(find "${HOME}"/pictures/wallpapers/ -type -f -print0 | shuf -z -n 1)
		feh --bg-scale "${PICTURE}"
		sleep 600
	done
}

function sendnote {
	curl -d "$1" hank.helm.styx/alerts
}

# aliases
alias n='doas nvim'
alias v='nvim'
alias nala='doas nala'
alias uu='doas nala update && doas nala upgrade -y'
alias ins='doas nala install -y'
alias se='doas nala search'
alias systemctl='doas systemctl'
alias sysctl='doas sysctl'
alias service='doas sysctl'
alias cat='batcat'
alias c='clear;source ~/.zshrc'
alias grep='grep --color'
alias egrep='egrep --color'
alias fgrep='fgrep --color'
alias stuff='v ~/.local/scripts/my_stuff.sh'
